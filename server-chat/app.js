var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var usuario = require('./routes/usuario');
var invUsuario = require('./routes/invitacion-usuario');

var app = express();

var http = require('http').Server(app);

app.io = require('socket.io')();  

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

//Origin Access 

app.use(function(request, response, next) {
      response.set({
        'Access-Control-Allow-Origin': 'http://127.0.0.1:8000',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Access-Control-Allow-Credentials': 'true'
    });

    next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/usuario', usuario);
app.use('/invitacion-usuario',invUsuario);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

//Socket.io
app.use(function(req, res, next){
  res.io = io;
  next();
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

/*-------Prueba chat--------*/

var messages = [{  
    author: "Carlos",
    text: "Hola! que tal?"
}];

app.io.on('connection', function(socket) {  
    console.log('Un usuario se ha conectado');

    socket.emit('messages', messages);

    socket.on('disconnect', function(){
      console.log('usuario desconectado');
    });

    socket.on('IdUsuario', function(id){
      console.log('joining room', id);
      socket.join(id);
    });

    socket.on('quieroConocerte', function(data){
      console.log('data quiero conocerte', JSON.stringify(data));

    });
    
});

module.exports = app;
