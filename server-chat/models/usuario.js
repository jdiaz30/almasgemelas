"use strict";

module.exports = function(sequelize, DataTypes) {
  var Usuario = sequelize.define("Usuario",{
     id: {
      type: DataTypes.INTEGER,
      field: 'id',
      primaryKey: true,
      autoIncrement: true
     },
     nombre: {
      type: DataTypes.STRING(30),
      field: 'nombre'
     },
     email: {
      type: DataTypes.STRING(100),
      field: 'email'
     },
     password: {
      type: DataTypes.TEXT,
      field: 'password'
     },
     estado: {
      type: DataTypes.INTEGER,
      field: 'estado'
     },
     fechaNac: {
      type: DataTypes.DATE,
      field: 'fecha_nac'
     },
     sexo : {
      type : DataTypes.STRING(1),
      field : 'sexo'
     },
     estCivil: {
      type: DataTypes.STRING(50),
      field: 'est_civil'
     },
     ciudadId: {
      type: DataTypes.INTEGER,
      field: 'ciudad_id'
      /*references: {
         model: 'Ciudad',
         key: 'id'
      }*/
     },
     titulo : {
      type : DataTypes.STRING(30),
      field : 'titulo'
     },
     descripcion : {
      type : DataTypes.STRING(350),
      field : 'descripcion'
     },
     fechaReg : {
      type : DataTypes.DATE,
      field : 'fecha_reg'
     },
     estatura : {
      type : DataTypes.STRING(35),
      field : 'estatura'
     },
     piel : {
      type : DataTypes.STRING(35),
      field : 'piel'
     },
     urlPerfil : {
      type : DataTypes.STRING(150),
      field : 'url_perfil'
     },
     educacion : {
       type : DataTypes.INTEGER,
       field : 'educacion'
     },
     ocupacion : {
      type : DataTypes.INTEGER,
      field : 'ocupacion'
     },
     ojos : {
      type : DataTypes.STRING(35),
      field : 'ojos'
     },
     fisico : {
      type : DataTypes.INTEGER,
      field : 'fisico'
     }

     
  }, 
  {
    classMethods: {
        getAll: function(){ return this.findAll(); }
    },

    freezeTableName: true, // Model tableName will be the same as the model 
    tableName: 'tb_usuario',
    timestamps: false,
      
  }
   
  );

  //User.sync({force: true});

  return Usuario;
};
