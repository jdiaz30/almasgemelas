"use strict";

module.exports = function(sequelize, DataTypes) {
  var InvitacionUsuario = sequelize.define("InvitacionUsuario",{
     id: {
      type: DataTypes.INTEGER,
      field: 'id',
      primaryKey: true,
      autoIncrement: true
     },
     usuarioEmi: {
      type: DataTypes.INTEGER,
      field: 'usuario_emi'
     },
     usuarioRec: {
      type: DataTypes.INTEGER,
      field: 'usuario_rec'
     },
     fecha: {
      type: DataTypes.DATE,
      field: 'fecha',
      defaultValue: DataTypes.NOW
     },
     estado: {
      type: DataTypes.INTEGER,
      field: 'estado'
     }
  }, 
  {
    classMethods: {
        getAll: function(){ return this.findAll(); }
    },

    freezeTableName: true, // Model tableName will be the same as the model 
    tableName: 'tb_invitacion_usuario',
    timestamps: false,   
  }
   
  );

  //User.sync({force: true});

  return InvitacionUsuario;
};
