var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/id/:id',function(req,res){
	models.Usuario.findAll({
		 where: { 
		 	id: req.params.id
		 } 
	}).then(function(data){
		//res.app.io.emit("socketToMe", "users");
		res.status(200).json(data);
	});          
});

module.exports = router;
