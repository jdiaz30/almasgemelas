var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('',function(req,res){

	models.InvitacionUsuario.create({
		usuarioEmi: req.body.usuario_emi,
		usuarioRec: req.body.usuario_rec,
		estado : 0
	}).then(function(data) {
		//res.app.io.emit("quiere", "users");
		console.log("usuario data " + data.id);
		var vIdInvitacion = data.id;

		models.Usuario.findAll({
			where: { 
				id: req.body.usuario_emi
			} 
		}).then(function(data){

			res.app.io.to(req.body.usuario_rec).emit('quieroConocerte', {
				titulo: "Quiere conocerte!",
				nombre: data[0].nombre,
				foto: req.body.usuario_emi_foto,
				url_perfil : data[0].urlPerfil,
				id_invitacion : vIdInvitacion
			});

			res.status(200).json("ok");
			
		});

	
	});   
});

router.put('',function(req,res){

	models.InvitacionUsuario.update({estado: 1}, 
		{where: {
			usuarioEmi: req.body.usuario_emi,
			usuarioRec: req.body.usuario_rec
		}
	}).then(function(data){

		console.log("aceptar usuario " + JSON.stringify(data));
		//var vIdInvitacion = data.id;

		models.Usuario.findAll({
			where: { 
				id: req.body.usuario_rec
			} 
		}).then(function(data){

			res.app.io.to(req.body.usuario_emi).emit('aceptarInvitacion', {
				titulo: "Ha aceptado tu invitación!",
				nombre: data[0].nombre,
				foto: req.body.usuario_emi_foto,
				url_perfil : data[0].urlPerfil,
				id_invitacion : ""
			});

			res.status(200).json("ok");
			
		});
		
	});
  
});

module.exports = router;
