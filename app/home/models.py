from django.db import models

#from panel.models import Ciudad

import panel

#estado = 0 , inhabilitado
#estado = 1 , habilitado

class Usuario(models.Model):
    nombre = models.CharField(max_length = 30)
    email = models.CharField(max_length = 100,unique = True)
    password = models.TextField()
    estado =  models.IntegerField()
    fecha_nac = models.DateField(null = True)
    sexo = models.CharField(max_length = 1,null = True)
    est_civil = models.CharField(max_length = 50,null = True)
    ciudad = models.ForeignKey('panel.Ciudad')
    titulo = models.CharField(max_length = 130)
    descripcion = models.CharField(max_length = 350)
    fecha_reg = models.DateField(null = True)
    estatura = models.CharField(max_length = 35, null = True)
    piel = models.CharField(max_length = 35 ,null = True)
    url_perfil = models.CharField(max_length = 150,null = True)
    educacion = models.IntegerField()
    ocupacion = models.IntegerField()
    ojos = models.CharField(max_length = 35,null = True)
    cabello = models.CharField(max_length = 35 , null = True)
    fisico = models.IntegerField()
    
    class Meta:
    	db_table = 'tb_usuario'

    def __unicode__(self):
        return self.nombre

class UsuarioAcceso(models.Model):
    fecha = models.DateTimeField(null = False)
    usuario = models.ForeignKey(Usuario)
    tipo = models.CharField(max_length = 1,null = False)
    ip = models.TextField()
    estado = models.IntegerField()

    class Meta:
        db_table = 'tb_usuario_acceso'

    def __unicode__(self):
        return self.usuario

class InvitacionUsuario(models.Model):
    usuario_emi = models.IntegerField()
    usuario_rec = models.IntegerField()
    fecha = models.DateTimeField()
    estado = models.IntegerField() # 0 = emitido , 1 = aceptado , 2 = visto (aceptado) , 3 = visto (emitido) 

    class Meta:
        db_table = 'tb_invitacion_usuario'

    def __unicode__(self):
        return self.estado
