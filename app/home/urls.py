from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$','home.views.home',name='home'),
 	url(r'^register/','home.views.register',name='register'),
 	url(r'^validate-account/(?P<email>[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4}))','home.views.validate_register',name='validate_register'),
    url(r'^login/','home.views.login',name='login'),
    url(r'^logout/','home.views.logout',name='logout'),
    url(r'^perfiles/','home.views.perfiles',name='perfiles'),
    url(r'^get-all-perfiles/$','home.views.listar_perfiles',name='listar_perfiles'),
    url(r'^perfil/(?P<url>[\w\-]+)','home.views.ver_perfil',name='ver_perfil'),
    url(r'^invitacion-usuario/','home.views.invitacion_usuario',name='invitacion_usuario'),
    url(r'^listar-invitacion-usuario/','home.views.listar_invitacion_usuario',name='listar_invitacion_usuario')
)