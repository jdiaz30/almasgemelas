# -*- coding: utf-8 -*-  
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse,HttpResponseRedirect

import json
import datetime
import time

from home.models import Usuario,UsuarioAcceso,InvitacionUsuario
from panel.models import Question,Religion,Media

from helper.fecha import *
from helper.datos import *
from helper.geo_ip import *
from panel.repository.media import *
from panel.repository.religion import *
from panel.repository.genero_musical import *

def perfiles_query(filtros,edad_ini,edad_fin,fila,paginador,id_usuario):
	data = Usuario.objects.select_related('ciudad__provincia__pais_question').filter(**filtros).exclude(id=id_usuario)[fila:paginador]
	usuario = []

	for user in data:
			edad = calcular_edad(user.fecha_nac)
			zodiaco = calcular_signo_zodiaco(str(user.fecha_nac))
			ciudad = user.ciudad.nombre +", "+ user.ciudad.provincia.nombre
			bandera = "flag flag-" + user.ciudad.provincia.pais.iso
			url_perfil = "/perfil/" + user.url_perfil
			
			fecha_act = datetime.date.today().strftime('%Y-%m-%d') 
			fecha_reg = datetime.datetime.strftime(user.fecha_reg, '%Y-%m-%d')
			fecha_dif = calcular_dias(fecha_act,fecha_reg)

			accesos_user = UsuarioAcceso.objects.filter(estado = 0,usuario_id = user.id).order_by('-fecha')[:1]

			foto_perfil = get_foto_perfil(user.id)

			fecha_acceso = ""
			for accesos in accesos_user:
				fecha_acceso = datetime.datetime.strftime(accesos.fecha, '%Y-%m-%d')

			fecha_acceso_final = calcular_dias(fecha_act,fecha_acceso) 
		
			res = {'nombre' : user.nombre,'email':user.email,'edad':edad,
			'zodiaco':zodiaco,'titulo': user.titulo ,'descripcion' : user.descripcion,'ciudad' : ciudad,'bandera' : bandera,
			'registrado' : fecha_dif,'acceso':fecha_acceso_final,'url_perfil': url_perfil,'foto_perfil':foto_perfil}

			if edad_ini!= "" and edad_fin:
				if edad >= edad_ini and edad <= edad_fin:
					usuario.append(res)
			else:
				usuario.append(res)

	return usuario

def perfil_detalle(url_perfil):
	usuario = Usuario.objects.get(url_perfil = url_perfil)

	edad = calcular_edad(usuario.fecha_nac)
	zodiaco = calcular_signo_zodiaco(str(usuario.fecha_nac))
	foto_perfil = get_foto_perfil(usuario.id)
	religion = get_religion_perfil(usuario.id)
	ciudad = usuario.ciudad.nombre +", "+ usuario.ciudad.provincia.nombre
	bandera = "flag flag-" + usuario.ciudad.provincia.pais.iso

	fotos = get_media_perfil(usuario.id,"image")
	musica = get_genero_musical_perfil(usuario.id)
	estado_civil = parser_estado_civil(usuario.est_civil)
	fisico = parser_fisico_usuario(str(usuario.fisico))
	educacion = parser_educacion(str(usuario.educacion))
	ocupacion = parser_ocupacion(str(usuario.ocupacion))

	res = {'id': usuario.id ,'nombre' : usuario.nombre,'titulo' : usuario.titulo,'descripcion' : usuario.descripcion,
	'edad' : edad , 'zodiaco' : zodiaco,'estatura' : usuario.estatura,'piel' : usuario.piel,'sexo': usuario.sexo,
	'foto_perfil' : foto_perfil,'religion' : religion,'ciudad' : ciudad,'bandera' : bandera,'fotos': fotos,
	'musica': musica,'est_civil': estado_civil,'cabello':usuario.cabello,'ojos': usuario.ojos,'fisico':fisico,
	'educacion':educacion,'ocupacion': ocupacion}

	return  res

def valida_invitacion_perfil(url_perfil,usuario_session,tipo):
	usuario_data = Usuario.objects.get(url_perfil = url_perfil)
	invitacion_data = []

	if tipo == "existe":
		invitacion_data = InvitacionUsuario.objects.filter(usuario_emi = usuario_session,usuario_rec = usuario_data.id)
	elif tipo == "pendiente":
		invitacion_data = InvitacionUsuario.objects.filter(usuario_emi = usuario_data.id,usuario_rec = usuario_session)
	else:
		invitacion_enviada = InvitacionUsuario.objects.filter(usuario_emi = usuario_session,usuario_rec = usuario_data.id,estado = 1)
		invitacion_recibida = InvitacionUsuario.objects.filter(usuario_emi = usuario_data.id,usuario_rec = usuario_session,estado = 1)

		if len(invitacion_enviada) > 0 or len(invitacion_recibida)>0:
			invitacion_data = [1]

	return len(invitacion_data)









