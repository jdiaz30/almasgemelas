
function signoZodiaco(vFecha){

	var vZodiaco = '';

	var dia = vFecha.substr(0,2);
	var mes = vFecha.substr(3,2);
	var anio = vFecha.substr(6,4);

	if     ( ( mes == 1 && dia > 19 )  || ( mes == 2 && dia < 19 ) )  { vZodiaco = "Acuario"; }
	else if ( ( mes == 2 && dia > 18 )  || ( mes == 3 && dia < 21 ) )  { vZodiaco = "Piscis"; } 
	else if ( ( mes == 3 && dia > 20 )  || ( mes == 4 && dia < 20 ) )  { vZodiaco = "Aries"; } 
	else if ( ( mes == 4 && dia > 19 )  || ( mes == 5 && dia < 21 ) )  { vZodiaco = "Tauro"; } 
	else if ( ( mes == 5 && dia > 20 )  || ( mes == 6 && dia < 21 ) )  { vZodiaco = "Géminis"; } 
	else if ( ( mes == 6 && dia > 20 )  || ( mes == 7 && dia < 23 ) )  { vZodiaco = "Cáncer"; } 
	else if ( ( mes == 7 && dia > 22 )  || ( mes == 8 && dia < 23 ) )  { vZodiaco = "Leo"; } 
	else if ( ( mes == 8 && dia > 22 )  || ( mes == 9 && dia < 23 ) )  { vZodiaco = "Virgo"; } 
	else if ( ( mes == 9 && dia > 22 )  || ( mes == 10 && dia < 23 ) ) { vZodiaco = "Libra"; } 
	else if ( ( mes == 10 && dia > 22 ) || ( mes == 11 && dia < 22 ) ) { vZodiaco = "Escorpio"; } 
	else if ( ( mes == 11 && dia > 21 ) || ( mes == 12 && dia < 22 ) ) { vZodiaco = "Sagitario"; } 
	else if ( ( mes == 12 && dia > 21 ) || ( mes == 1 && dia < 20 ) )  { vZodiaco = "Capricornio"; } 
 
   return vZodiaco; 
}

function getDateNow(){

	var vNow = moment().format("YYYY-MM-DD HH:mm:ss");
	return vNow;
}

function calcularEdad(vFecha){

	var dia = vFecha.substr(0,2);
	var mes = vFecha.substr(3,2);
	var anio = vFecha.substr(6,4);

	fecha_hoy = new Date();
	ahora_anio = fecha_hoy.getYear();
	ahora_mes = fecha_hoy.getMonth();
	ahora_dia = fecha_hoy.getDate();
	edad = (ahora_anio + 1900) - anio;

	if ( ahora_mes < (mes - 1)){
		edad--;
	}

	if (((mes - 1) == ahora_mes) && (ahora_dia < dia)){ 
		edad--;
	}

	if (edad > 1900){
		edad -= 1900;
	}

	return edad;
}

function convertFechaYmd(vFecha){

	var vFechaYmd = "";

	var dia = vFecha.substr(0,2);
	var mes = vFecha.substr(3,2);
	var anio = vFecha.substr(6,4);

	vFechaYmd = anio +"-"+mes+"-"+dia;

	return vFechaYmd;

}

function getValueEducacion(value){
	var vEducacion = ['No especificado','Instituto de Enseñanza Superior','Estudiante Universitario/a','Titulo Universitario',
	'Estudios PostGrado/Master','Entrenamiento Técnico','Diplomado Universitario'];

	return vEducacion[value];
}

function getValueOcupacion(value){
	var vOcupacion = ['No especificado','Independiente/Autónomo','Manager','Profesional','Ejecutivo','Administrativo','Ventas',
	'Servicio al Usuario','Comerciante','Artista','Profesor','Ama de Casa/En casa con los hijos','Desempleado'];

	return vOcupacion[value];
}

function getValueFisico(value){
	var vFisico = ['Elegir','Delgado/a','Esbelto/a','Musculoso/a'];

	return vFisico[value];
}