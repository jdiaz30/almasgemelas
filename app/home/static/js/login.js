$(function() {
	var vLogin = {
		init : function(){

			$(".btn-register").click(function(){

				vLogin.login();

			});

		},
		login : function(){

			var vData = $("#frmLogin").serialize();

			$.ajax({
                url:"/login/",  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){

                	$("#loading").fadeIn(100);
                 
                },
                success:function(respuesta){

                	$("#loading").fadeOut(100,function(){

                		if (respuesta == "0") {

                		   location.href="/panel";
                		}else{
                			sweetAlert("No se pudo ingresar", "Sus credenciales no son correctas", "error");
                		}
                		
                	});

                }
            });
		}
	
	};

	vLogin.init();
});