$(function() {

	var vPerfil = {
		init : function(){

			ko.applyBindings(new vViewModel.masterViewModel(),document.getElementById("perfil"));
		},

	};

	var vViewModel = {
		masterViewModel : function(){
			this.perfil =  vPerfilModel;
		}	
	};

	var vPerfilModel = {
		label_invite : ko.observable("Quiero conocerte "),
		label_accept_invite : ko.observable("Aceptar invitación"),
		nclick_invite : ko.observable(0),
		quieroConocerte : function(){
			var vData = {
				usuario_emi: $("#usuario_session").val(),
				usuario_rec: $("#usuario_perfil").val(),
				usuario_emi_foto: $("#foto_perfil").val()
			};

			var self = this;

			var vLoadingButton = Ladda.create( document.querySelector( '.ladda-button' ) );

			if (self.nclick_invite() === 0) {

				$.ajax({
					url: URL_SERVER_CHAT + "/invitacion-usuario",  
					type:'POST',
					data :vData,
					async:true, 
					dataType : 'json',
					beforeSend : function(){
						vLoadingButton.start();
					},
					success:function(respuesta){
						vLoadingButton.stop();
						self.label_invite("Invitacion enviada ");
						console.log(JSON.stringify(respuesta));
					}
				});

				var previousCount = self.nclick_invite();
				self.nclick_invite(previousCount + 1);
			}
		},
		aceptarInvitacion : function(){
			var vData = {
				usuario_emi : $("#usuario_perfil").val(),
				usuario_rec : $("#usuario_session").val(),
				usuario_emi_foto: $("#foto_perfil").val()
			};

			var self = this;

			var vLoadingButton = Ladda.create( document.querySelector( '.ladda-button' ) );

			if (self.nclick_invite() === 0) {

				$.ajax({
					url: URL_SERVER_CHAT + "/invitacion-usuario",  
					type:'PUT',
					data :vData,
					async:true, 
					dataType : 'json',
					beforeSend : function(){
						vLoadingButton.start();
					},
					success : function(respuesta){
						vLoadingButton.stop();
						self.label_accept_invite("Ahora son contacto! ");

						console.log(JSON.stringify(respuesta));
					},
					error : function(error){
						vLoadingButton.stop();
						console.log(JSON.stringify(error));
					}
				});

				var previousCount = self.nclick_invite();
				self.nclick_invite(previousCount + 1);
			}
		}
	};

	vPerfil.init();
});