$(function() {
	var vReligion = "";
	var vPiel = [];
	var vEstatura = "";

	var vPerfiles = {
		init : function(){

			$('[data-toggle="checkbox"]').radiocheck();
			$('[data-toggle="radio"]').radiocheck();

			$("[name=religion]").click(function(){

				vReligion = ($(this).val() == "5") ? "" : $(this).val() ;
			
				var vParams = {
					religion : vReligion,
					piel : (vPiel.length === 0) ? "" : vPiel,
					estatura : vEstatura,
					sexo : $("#sexo-interes option:selected").val(),
					edad_ini : $("#edad_ini option:selected").val(),
					edad_fin : $("#edad_fin option:selected").val(),
					fila : '0',
					paginador : '10',
					ciudad : $("#ciudad option:selected").val()
				};

				vPerfiles.listarPerfiles(vParams,false);
			});

			$("[name=piel]").click(function(){
				var vValue = $("[name=piel]").serializeArray();

				vPiel.splice(0, vPiel.length);

				$( vValue ).each(function(index,value) {
					
					vPiel.push(value.value);
				});

				var vParams = {
					religion : vReligion,
					piel : (vPiel.length === 0) ? "" : vPiel,
					estatura : vEstatura,
					sexo : $("#sexo-interes option:selected").val(),
					edad_ini : $("#edad_ini option:selected").val(),
					edad_fin : $("#edad_fin option:selected").val(),
					fila : '0',
					paginador : '10',
					ciudad : $("#ciudad option:selected").val()
				};

				console.log(JSON.stringify(vPiel));


				vPerfiles.listarPerfiles(vParams,false);
			});

			$("[name=estatura]").click(function(){
				vEstatura = $(this).val();

				var vParams = {
					religion : vReligion,
					piel : (vPiel.length === 0) ? "" : vPiel,
					estatura : vEstatura,
					sexo : $("#sexo-interes option:selected").val(),
					edad_ini : $("#edad_ini option:selected").val(),
					edad_fin : $("#edad_fin option:selected").val(),
					fila : '0',
					paginador : '10',
					ciudad : $("#ciudad option:selected").val()
				};

				vPerfiles.listarPerfiles(vParams,false);
			});

			$("#btn-buscar").click(function(){
				var vParams = {
					religion : vReligion,
					piel : (vPiel.length === 0) ? "" : vPiel,
					estatura : vEstatura,
					sexo : $("#sexo-interes option:selected").val(),
					edad_ini : $("#edad_ini option:selected").val(),
					edad_fin : $("#edad_fin option:selected").val(),
					fila : '0',
					paginador : '10',
					ciudad : $("#ciudad option:selected").val()
				};

				vPerfiles.listarPerfiles(vParams);
			});

			ko.applyBindings(new vViewModel.masterViewModel(),document.getElementById("perfiles"));

			vPerfiles.listarPerfiles(
				{
					religion : '',
					piel : '',
					estatura : '',
					sexo : $("#sexo-interes option:selected").val(),
					edad_ini : '',
					edad_fin : '',
					fila : '0',
					paginador : '10',
					ciudad : $("#ciudad option:selected").val()
				},
				false
			);	
		},
		listarPerfiles : function(vData,vAdd){
			 $.ajax({
                url:"/get-all-perfiles/",  
                type:'POST',
                data :vData,
                async:true, 
                dataType : 'json',
                beforeSend : function(){
                	$(".fondo-loading").fadeIn(250);
                	$(".loading").fadeIn(50);
                },
                success:function(respuesta){

                	$(".fondo-loading").fadeOut(250);
                	$(".loading").fadeOut(50);
                	
                	var vJson = JSON.stringify(respuesta);

                	if(vJson.indexOf("data") != -1){

                		if(vAdd === true){
                			console.log(JSON.stringify(respuesta[0].data));

                			$(respuesta[0].data).each(function(index,data){
                				vPerfilesVM.perfiles.push(data);
                			});
                			
                		}else{
                			vPerfilesVM.perfiles(respuesta[0].data);	
                		}

                	}

                	if(vJson.indexOf("fila") != -1){
                		vPerfilesVM.fila(respuesta[1].fila);
                	}else{
                		vPerfilesVM.fila("");
                	}

                	if(vJson.indexOf("paginador") != -1){
                		vPerfilesVM.paginador(respuesta[2].paginador);
                	}else{
                		vPerfilesVM.paginador("");
                	}

                }
            });
		}
		
	};

	var vViewModel = {
		masterViewModel : function(){
			this.verPerfiles =  vPerfilesVM;
			this.header = vHeader;
		},
		
	};

	var vHeader = {
		foto_perfil : ko.observable($("#foto_perfil").val())
	};

	var vPerfilesVM = {
		perfiles : ko.observableArray([]),
		fila : ko.observable(""),
		paginador : ko.observable(""),
		verMas : function(){
				var vParams = {
					religion : vReligion,
					piel : (vPiel.length === 0) ? "" : vPiel,
					estatura : vEstatura,
					sexo : $("#sexo-interes option:selected").val(),
					edad_ini : $("#edad_ini option:selected").val(),
					edad_fin : $("#edad_fin option:selected").val(),
					fila : $(this).attr("fila"),
					paginador : $(this).attr("paginador"),
					ciudad : $("#ciudad option:selected").val()
				};

				vPerfiles.listarPerfiles(vParams,true);
		}
	};

	
	vPerfiles.init();
});