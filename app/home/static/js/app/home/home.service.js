
var Home = function(){

  var self = this;

  return {
    registrar : function(formData){
        var defer = $.Deferred();

        $.ajax({
          url:"/register/",  
          type:'POST',
          async:true, 
          data: formData,
          beforeSend:function(respuesta){
            defer.notify(respuesta);
          },
          success:function(respuesta){
            defer.resolve(respuesta);
          },
          error : function(respuesta){
            defer.reject(respuesta);
          }
        });

        return  defer.promise();
    },
    getAllPais : function(){
      var defer = $.Deferred();

      $.ajax({
        url:"/panel/get-all-pais/",  
        type:'POST',
        async:true, 
        data: {},
        beforeSend:function(respuesta){
          defer.notify(respuesta);
        },
        success:function(respuesta){
          defer.resolve(respuesta);
        },
        error : function(respuesta){
          defer.reject(respuesta);
        }
      });

      return defer.promise();
    },
    getAllProvincia : function(paisId){
      var defer = $.Deferred();

      $.ajax({
        url:"/panel/get-prov-by-pais/"+paisId,  
        type:'GET',
        async:true, 
        data: {},
        beforeSend:function(respuesta){
          defer.notify(respuesta);
        },
        success:function(respuesta){
          defer.resolve(respuesta);
        },
        error : function(respuesta){
          defer.reject(respuesta);
        }
      });

      return defer.promise();
    },
    getAllCiudad : function(provId){
      var defer = $.Deferred();

      $.ajax({
        url:"/panel/get-ciudad-by-prov/"+provId,  
        type:'POST',
        async:true, 
        data: {},
        beforeSend:function(respuesta){
          defer.notify(respuesta);
        },
        success:function(respuesta){
          defer.resolve(respuesta);
        },
        error : function(respuesta){
          defer.reject(respuesta);
        }
      });

      return defer.promise();
    }
  };

};

module.exports = Home;  