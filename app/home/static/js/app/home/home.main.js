var homeService = require('./home.service');
//var homePlugin = require('./home.plugin');

$(function() {
	var home = new homeService();

	var init = function(){
		listPais();

		ko.applyBindings(registroUsuarioVM,document.getElementById("register"));
	};

    var listPais = function(){
    	$.when( home.getAllPais() ).then(
    		function( response ) {
    			registroUsuarioVM.pais.data(response);
    			//listProvincia(response[1].value);

    			console.log( JSON.stringify(response,true) + ", success" );

    		},
    		function( response ) {
    			console.log( JSON.stringify(response,true) + ", error" );
    		},
    		function( response ) {
    			console.log( JSON.stringify(response,true) + ", notify" );
    		}
    	);
    };

    var listProvincia = function(paisId){
    	$.when( home.getAllProvincia(paisId) ).then(
    		function( response ) {
    			registroUsuarioVM.provincia.data(response);
    			console.log( JSON.stringify(response,true) + ", success" );
    		},
    		function( response ) {
    			console.log( JSON.stringify(response,true) + ", error" );
    		},
    		function( response ) {
    			console.log( JSON.stringify(response,true) + ", notify" );
    		}
    	);
    };

    var listarCiudad = function(provId){

    	$.when( home.getAllCiudad(provId) ).then(
    		function( response ) {
    			registroUsuarioVM.ciudad.data(response);
    			console.log( JSON.stringify(response,true) + ", success" );
    		},
    		function( response ) {
    			console.log( JSON.stringify(response,true) + ", error" );
    		},
    		function( response ) {
    			console.log( JSON.stringify(response,true) + ", notify" );
    		}
    	);
    };

    var registroUsuarioVM  = {
		pais : {
			data : ko.observableArray([]),
			selected : ko.observable()
		},
		provincia : {
			data : ko.observableArray([]),
			selected : ko.observable()
		},
		ciudad :  {
			data : ko.observableArray([]),
			selected : ko.observable()
		},
        registrar : function(){
        	$.when( home.registrar({}) ).then(
        		function( response ) {
        			console.log( response + ", success" );
        		},
        		function( response ) {
        			console.log( response + ", error" );
        		},
        		function( response ) {
        			console.log( response + ", notify" );
        		}
        	);
        },
        changePais : function(){

        	var paisId = this.pais.selected();

        	if(paisId !== undefined){
        		listProvincia(this.pais.selected());
        	}
        	
        },
        changeProv : function(){
  
        	var provId = this.provincia.selected();

        	if(provId !== undefined){
        		listarCiudad(provId);
        	}
        }    
    };

	init();

	
});