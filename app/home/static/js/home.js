$(function() {
	var vRegister = {
		init : function(){

			$(".btn-register").click(function(){

				vRegister.registrar();

			});

            $("#pais").change(function(){
                vRegister.getAllProvincia($("#pais option:selected").val());
            });

            $("#prov").change(function(){
                vRegister.getAllCiudad($("#prov option:selected").val());
            });

            vRegister.getAllPais();

		},
		registrar : function(){

			var vData = $("#frmRegister").serialize();

			$.ajax({
                url:"/register/",  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){

                	$("#loading").fadeIn(100);
                 
                },
                success:function(respuesta){

                	$("#loading").fadeOut(100,function(){

                		if (respuesta == "0") {

                			$("#form").fadeOut(100,function(){
                				$("#msg-register").fadeIn(100);
                			});

                		}else{
                			sweetAlert("No se pudo registrar", " El email ingresado ya existe", "error");
                		}
                		
                	});

                }
            });
		},
        getAllPais : function(){

            $.ajax({
                url:"/panel/get-all-pais/",  
                type:'POST',
                async:true, 
                data: {},
                beforeSend:function(){

                },
                success:function(data){

                   $('#pais').html("");

                   $(data).each(function (item, data) {

                        $('#pais')
                        .append($("<option></option>")
                        .attr("value", this.value)
                        .text(this.text));
                    });

                   vRegister.getAllProvincia($("#pais option:selected").val());

                }
            });
        },
        getAllProvincia : function(vPais){

            $.ajax({
                url:"/panel/get-prov-by-pais/"+vPais,  
                type:'GET',
                async:true, 
                data: {},
                beforeSend:function(){

                },
                success:function(data){

                   $('#prov').html("");

                   $(data).each(function (item, data) {

                        $('#prov')
                        .append($("<option></option>")
                        .attr("value", this.value)
                        .text(this.text));
                    });

                    vRegister.getAllCiudad($("#prov option:selected").val());

                }
            });
        },
        getAllCiudad : function(vProvincia){
            $.ajax({
                url:"/panel/get-ciudad-by-prov/"+vProvincia,  
                type:'GET',
                async:true, 
                data: {},
                beforeSend:function(){

                },
                success:function(data){

                   $('#ciudad').html("");

                   $(data).each(function (item, data) {

                        $('#ciudad')
                        .append($("<option></option>")
                        .attr("value", this.value)
                        .text(this.text));
                    });

                }
            });
        }
	
	};

	vRegister.init();
});