$(function() {
	
	URL_SERVER_CHAT = "http://127.0.0.1:3000";
	URL_SERVER = "http://127.0.0.1:8000";
	socketServer = io.connect(URL_SERVER_CHAT);

	var vSessionUsuario = {
		init : function(){

			socketServer.emit('IdUsuario', $("#usuario_session").val());

			socketServer.on('quieroConocerte', function(data) {  
				console.log(JSON.stringify(data));
				vSessionUsuario.addNotificacion(data);
			});

			socketServer.on('aceptarInvitacion', function(data) {  
				console.log(JSON.stringify(data));
				vSessionUsuario.addNotificacion(data);
			});

			ko.applyBindings(new vViewModel.masterViewModel(),document.getElementById("notify-vm"));
			vSessionUsuario.linkNotificacion();	
			vSessionUsuario.listarNotificaciones();

		},
		addNotificacion : function(data){
			var vNotify = {
				nombre : data.nombre,
				titulo : data.titulo,
				foto : data.foto,
				url_perfil : URL_SERVER+"/perfil/"+data.url_perfil,
				id_invitacion : data.id_invitacion
			};
			var vItemLast = vNotificacion.item();
			vNotificacion.item(vItemLast + 1);
			vNotificacion.data.push(vNotify);
		},
		linkNotificacion : function(){
			$(document).on('click','.list-group-item.notify-friend', function(e) {
				e.preventDefault();
				var vUrl = $(this).attr("href");
				var vId = $(this).attr("data");

				vSessionUsuario.verUrlPerfilNotificacion(vUrl,vId);
			});
		},
		verUrlPerfilNotificacion : function(url,id){
			$.ajax({
                url: URL_SERVER + "/invitacion-usuario/",  
                type:'POST',
                data :{
                	id : id
                },
                async:true, 
                beforeSend : function(){
                },
                success:function(respuesta){
                	location.href = url;
                },
                error:function(error){
                	alert(JSON.stringify(error));
                }
            });
		},
		listarNotificaciones : function(){
			$.ajax({
				url: URL_SERVER + "/listar-invitacion-usuario/",  
				type:'GET',
				data :{},
				async:true, 
				dataType : 'json',
				beforeSend : function(){
				
				},
				success:function(respuesta){
					
					$( respuesta ).each(function(index,value) {
						vSessionUsuario.addNotificacion(value);
					});
				}
			});
		}
	};

	var vViewModel = {
		masterViewModel : function(){
			this.notificacion =  vNotificacion;
		}	
	};

	var vNotificacion = {
		data : ko.observableArray([]),
		item : ko.observable(0)
	};

	vSessionUsuario.init();
});