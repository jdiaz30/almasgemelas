# -*- coding: utf-8 -*-  
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse,HttpResponseRedirect

from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.db.models import Q

import json
import datetime
import time

from home.models import Usuario,UsuarioAcceso,InvitacionUsuario
from panel.models import Question,Religion,Pais,Provincia,Ciudad

from home.repository.usuario import *
from panel.repository.media import *
from helper.datos import *

from django.contrib.auth.hashers import make_password,check_password


def home(request):
	header = "base.html"

	if "usuario" in request.session != None:
		header = "base_panel.html"

	return render_to_response("home.html",{'header' : header,'session':request})

def register(request):
	if request.is_ajax():

		try:

			password = make_password(request.POST['pass'])

			usuario = Usuario(nombre = request.POST['nombre'], email = request.POST['email'], password = password,estado=0,
				fecha_nac = None,sexo = None,est_civil = None,ciudad_id = request.POST['ciudad'] ,fecha_reg = datetime.date.today().strftime('%Y-%m-%d')  )
			usuario.save()

			subject, from_email, to = 'Almas Gemelas : Bienvenido', 'joper30@gmail.com', request.POST['email']

			url_validate = settings.SERVER_PROD + '/validate-account/' + request.POST['email'] 

			html_content = 'Bienvenido '+request.POST['nombre']+' a Almas Gemelas <br>'
			html_content += 'Necesita validar su cuenta para poder ingresar. <br>'
			html_content += '<a href="'+url_validate+'">validar tu cuenta</a>'

			msg = EmailMultiAlternatives(subject, html_content, from_email, [to])

			msg.attach_alternative(html_content, "text/html")
			msg.send()

			return HttpResponse("0")
		except Exception, e:
			return HttpResponse(e,status=406)
	
	else:
		if "usuario" in request.session:
			return redirect('/panel')
		else :
			return render_to_response("register.html")

def validate_register(request,email = None):
	
	try:

		usuario = Usuario.objects.get(email = email,estado = 0)

		Usuario.objects.filter(email=email ).update(estado=1)
		question = Question(usuario_id = usuario.id,geo_quest = None ,geo = None,interesado = None,cocina = None,religion_id = None , fiesta = None)
		question.save()

		return render_to_response("validate-account.html",{'exito':'si'})

	except Usuario.DoesNotExist:

		return render_to_response("validate-account.html",{'exito':'no'})

def login(request):

	if request.is_ajax():

		usuario = Usuario.objects.filter(email = request.POST['email'],estado = 1)
		totalUser = usuario.count()
		password = ''

		if totalUser > 0:

			for item in usuario:
				password = item.password
				id_usuario = item.id
				nombre = item.nombre

			if check_password(request.POST['pass'],password):

				request.session["usuario"] = request.POST['email']
				request.session["id"] = id_usuario
				request.session["nombre"] = parser_nombre_session(nombre)
				request.session["image"] = get_foto_perfil(id_usuario)

				fecha = datetime.datetime.now()
				ip = get_client_ip(request)

				acceso_register = UsuarioAcceso(usuario_id = id_usuario,ip = ip, fecha = fecha ,tipo = "i",estado =0)
				acceso_register.save()

				return HttpResponse("0")
			else:
				return HttpResponse("1")
				
		else:
			return HttpResponse("1")

	else:
		if "usuario" in request.session:
			return redirect('/panel')
		else:
			return render_to_response("login.html")

def logout(request):

	try:
		fecha = datetime.datetime.now()
		ip = get_client_ip(request)

		acceso_register = UsuarioAcceso(usuario_id = request.session['id'],ip = ip, fecha = fecha ,tipo = "s",estado = 0)
		acceso_register.save()

		del request.session['usuario']
		del request.session['id']
		del request.session['nombre']
		del request.session['image']

		return HttpResponseRedirect("/")
	except KeyError:
		pass
		return HttpResponseRedirect("/")

def perfiles(request):
	header = "base.html"

	if "usuario" in request.session != None:
		header = "base_panel.html"

	ciudad_data = Ciudad.objects.filter(estado = 0,provincia_id = 1)

	religion_data = Religion.objects.filter(estado = 0)

	edad_range_ini = range(18, 45)
	edad_range_fin = range(45, 72)

	return render_to_response("perfiles.html",
		{'header' : header,'religion' : religion_data,'edad_ini' : edad_range_ini,'edad_fin': edad_range_fin,'ciudad' : ciudad_data,'session':request})

def listar_perfiles(request):
	usuario = []
	#data.query imprime el sql
	if request.is_ajax():

		my_filter = {}
		my_filter['estado'] = 1
		my_filter['fecha_nac__isnull'] = False
		my_filter['url_perfil__isnull'] = False
		my_filter['sexo__isnull'] = False

		if request.POST['religion'] != None and request.POST['religion'] != "":

			my_filter['question__religion_id'] = request.POST['religion']

		if request.POST.get('piel',False) != None and request.POST.get('piel',False) != "":

			my_filter['piel__in'] = request.POST.getlist('piel[]')

		if request.POST['estatura'] != None and request.POST['estatura'] != "":

			my_filter['estatura'] = request.POST['estatura']

		if request.POST['sexo'] != None and request.POST['sexo'] != "":

			my_filter['sexo'] = request.POST['sexo'] 

		if request.POST['ciudad'] != None and request.POST['ciudad'] != "":
			my_filter['ciudad_id'] = request.POST['ciudad']

		perfiles =  perfiles_query(my_filter,request.POST['edad_ini'],request.POST['edad_fin'],request.POST['fila'],request.POST['paginador'],request.session['id'])

		fila = int(request.POST['fila']) + 10
		paginador = int(request.POST['paginador']) + 10

		perfiles_next = perfiles_query(my_filter,request.POST['edad_ini'],request.POST['edad_fin'],fila,paginador,request.session['id'])

		usuario.append({'data':perfiles})

		if len(perfiles_next) > 0:
			usuario.append({'fila':fila})
			usuario.append({'paginador':paginador})

	return  HttpResponse(json.dumps(usuario), mimetype="application/json")

def ver_perfil(request,url = None):
	header = "base.html"

	if "usuario" in request.session != None:
		header = "base_panel.html"

	usuario_data = perfil_detalle(url)
	invite_perfil = valida_invitacion_perfil(url,request.session['id'],"existe")
	pend_invite = valida_invitacion_perfil(url,request.session['id'],"pendiente")
	invite_aceptado = valida_invitacion_perfil(url,request.session['id'],"aceptado")

	return render_to_response("perfil.html",{'header' : header,'session':request,'usuario': usuario_data,
		'invite_perfil' : invite_perfil,'pend_invite_perfil' : pend_invite,'invite_aceptado' : invite_aceptado})

def invitacion_usuario(request):
	if request.is_ajax():
		try:
			InvitacionUsuario.objects.filter(id = request.POST["id"] ).update(estado = 3)
			return HttpResponse("0")
		except Exception, e:
			return HttpResponse("error")
	return HttpResponse("0")#ok	

def listar_invitacion_usuario(request):
	if request.is_ajax():
		try:
			data = InvitacionUsuario.objects.filter(usuario_rec = request.session['id'],estado = 0)
			data_json = []
			for invitacion in data:
				usuario_data = Usuario.objects.get(id = invitacion.usuario_emi)
				json_invite = {
					'nombre' : usuario_data.nombre,
					'foto' : get_foto_perfil(usuario_data.id), 
					'url_perfil' : usuario_data.url_perfil,
					'id_invitacion' : invitacion.id,
					'titulo' : 'Quiere conocerte!'
				}
				data_json.append(json_invite)

			return  HttpResponse(json.dumps(data_json), mimetype="application/json")
		except Exception, e:
			return HttpResponse(e)	
		
	return HttpResponse("0")#ok	




















		
	



