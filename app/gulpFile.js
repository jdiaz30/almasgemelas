
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    stylus = require('gulp-stylus'),
    nib = require('nib'),
    gulpif = require('gulp-if'),
    minifyCss = require('gulp-minify-css'),
    useref = require('gulp-useref'),
    rename = require('gulp-rename'),
    buffer = require('vinyl-buffer'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify');


gulp.task('browserify', function() {  
  return browserify('./home/static/js/app/home/home.main.js')
    .bundle()
    .pipe(source('home.bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./home/static/js/app/home'));
});

// Preprocesa archivos Stylus a CSS y recarga los cambios
gulp.task('css', function() {
 gulp.src('./home/static/css/*.styl')
 	.pipe(stylus({ use: nib() }))
 	.pipe(gulp.dest('./home/static/css/'))
 	//.pipe(connect.reload());
});

gulp.task('mincss', function() {
    gulp.src(['./home/static/css/style.css'])
        .pipe(minifyCss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./home/static/css/'))
});


//Automatizamos esta tarea
gulp.task('watch', function(){
    gulp.watch(['./home/static/css/*.styl'], ['css']);
    gulp.watch(['./home/static/css/*.css'], ['mincss']);
    gulp.watch(['./home/static/js/app/home/*.js'], ['browserify']);

});

//ejecutamos el servidor y todos los archivos
gulp.task('default', ['watch','css','browserify']);
