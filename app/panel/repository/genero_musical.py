
import json
import datetime
import time

from home.models import Usuario,UsuarioAcceso
from panel.models import Question,GeneroMusical,GeneroMusicalQuestion

from helper.fecha import *
from helper.geo_ip import *

def get_genero_musical_perfil(usuario_id):
	question_perfil = Question.objects.get(usuario_id = usuario_id)
	musica_perfil = GeneroMusicalQuestion.objects.filter(question_id = question_perfil.id).select_related("genero_musical")

	return musica_perfil