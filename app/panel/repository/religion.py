# -*- coding: utf-8 -*-  
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse,HttpResponseRedirect

import json

from panel.models import Religion,Question
from home.models import Usuario

def get_religion_perfil(usuario_id):
	question = Question.objects.get(usuario_id = usuario_id)

	religion = question.religion

	return religion



