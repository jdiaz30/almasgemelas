# -*- coding: utf-8 -*-  
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse,HttpResponseRedirect

from home.models import Usuario,InvitacionUsuario
from panel.repository.media import *

def get_pretendientes(id_usuario):
	data = []
	try:
		dataReceptor = InvitacionUsuario.objects.filter(usuario_rec = id_usuario,estado = 1)
		dataEmisor = InvitacionUsuario.objects.filter(usuario_emi = id_usuario,estado = 1)

		pretendientes = depurar_data_pretendientes(dataReceptor,dataEmisor,id_usuario)

		for usuario in pretendientes:
			data_usuario = Usuario.objects.get(id = usuario['id'])
			foto_perfil = get_foto_perfil(usuario['id'])
			res = { 'nombre' : data_usuario.nombre,'foto' : foto_perfil}
			data.append(res)

		return data
	except Exception, e:
		print(e)
		return HttpResponse(e)	

	return data

def depurar_data_pretendientes(dataReceptor,dataEmisor,id_usuario):
	contact = []
	for item in dataReceptor:
		res = {'id' : item.usuario_emi}
		contact.append(res)

	for item in dataEmisor:
		res = {'id' : item.usuario_rec}
		contact.append(res)

	return contact
