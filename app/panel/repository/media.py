# -*- coding: utf-8 -*-  
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse,HttpResponseRedirect

import json

from panel.models import Media

def define_foto_perfil(media_id,usuario):
	Media.objects.filter(usuario_id = usuario).update(perfil = "no")
	Media.objects.filter(id = media_id,usuario_id = usuario).update(perfil = "si")

	foto = get_foto_perfil(usuario)

	return foto

def get_foto_perfil(usuario_id):
	foto_perfil = ""
	try:
		media = Media.objects.get(usuario_id = usuario_id,perfil = "si",tipo = "image")

		foto_perfil = media.link.name + ".400x300_q85_crop.jpg"

		foto_perfil = foto_perfil.replace("panel", "") 

	except Media.DoesNotExist:
		foto_perfil = "/static/img/face2.jpg"

	return foto_perfil

def get_media_perfil(usuario_id,tipo):
	media_photos = ""

	try:
		media_photos = Media.objects.filter(usuario_id = usuario_id,estado = 0,tipo = tipo).exclude(perfil="si")

	except Media.DoesNotExist:
		media_photos = ""

	return media_photos

	


