from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse,HttpResponseRedirect 

import json

from panel.models import Pais,Provincia,Ciudad,GeneroMusical,Religion,Question,GeneroMusicalQuestion,Media
from home.models import Usuario

from forms import UploadFileForm
from django.template.defaultfilters import slugify

from panel.repository.media import *
from panel.repository.invitacion_usuario import *
from helper.datos import *

def index(request):

	try:

		usuario = Usuario.objects.filter(email = request.session["usuario"] ,estado = 1).select_related('ciudad__provincia__pais')

		religion = Religion.objects.filter(estado = 0)

		id_usuario = request.session["id"]

		question = Question.objects.get(usuario_id = id_usuario)

		gm_question = GeneroMusicalQuestion.objects.filter(question_id = question.id).select_related('musica')

		media_photos = Media.objects.filter(usuario_id = id_usuario,estado = 0)

		foto_perfil = get_foto_perfil(id_usuario)

		return render_to_response("panel.html",{'usuario' : usuario,'religion' : religion,'question' : question,
			'gm_question' : gm_question,'media_photos' : media_photos,'foto_perfil': foto_perfil,'session':request})

	except Exception, e:
		return redirect('/')

def get_all_pais(request):

	data = Pais.objects.filter(estado = 0)
	pais = []

	for item in data:
		res = {'text' : item.nombre,'value':item.id}
		pais.append(res)

	return  HttpResponse(json.dumps(pais), mimetype="application/json" )

def get_prov_by_pais(request, pais = None):

	data = Provincia.objects.filter(pais_id = pais,estado = 0)
	prov = []

	for item in data:
		res = {'text': item.nombre , 'value': item.id}

		prov.append(res)

	return HttpResponse(json.dumps(prov), mimetype="application/json")

def get_ciudad_by_prov(request, prov = None):

	data = Ciudad.objects.filter(provincia_id = prov,estado = 0)
	ciudad = []

	for item in data:
		res = {'text': item.nombre , 'value': item.id}

		ciudad.append(res)

	return HttpResponse(json.dumps(ciudad), mimetype="application/json")

def get_genero_musical(request):

	data = GeneroMusical.objects.filter(estado = 0)
	musica = []

	for item in data:
		res = {'text' : item.descripcion,'value':item.id}
		musica.append(res)

	return HttpResponse(json.dumps(musica), mimetype="application/json" ) 

def upload_image(request):

	if request.is_ajax():
		form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():

        	new_media = Media(usuario_id = request.session["id"] ,link = form.cleaned_data['image'] ,tipo = "image" ,estado = 0)
        	new_media.save()

        	upload_file = Media.objects.latest()

        	thumb_url = upload_file.link['avatar'].url

        	thumb_url = thumb_url.replace("panel", "")

        	media_json = {'id' : upload_file.id,'link' : thumb_url}

        	return HttpResponse(json.dumps(media_json), mimetype="application/json")#retornamos la url de la imagen
        else:
            return HttpResponse("0")

def delete_media(request):

	if request.is_ajax():

		media_id = request.POST['media_id']

		Media.objects.filter(id = media_id).update(estado = 1)

		return HttpResponse("0")

def edit_usuario(request):

	if request.is_ajax():

		url_perfil = slugify(request.POST['nombre'])

		Usuario.objects.filter(id = request.session["id"] ).update(nombre = request.POST['nombre'],
			fecha_nac = request.POST['fecha_nac'],sexo = request.POST['sexo'],est_civil = request.POST['est_civil'],
			ciudad = request.POST['ciudad'] ,titulo = request.POST['titulo'],descripcion = request.POST['descripcion'],
			estatura = request.POST['estatura'],piel = request.POST['piel'],url_perfil = url_perfil,educacion = request.POST['educacion'],
			ocupacion = request.POST['ocupacion'],ojos = request.POST['ojos'],cabello = request.POST['cabello'],
			fisico = request.POST['fisico'])

		request.session["nombre"] = parser_nombre_session(request.POST['nombre'])

		return HttpResponse("0")

def edit_question(request):

	if request.is_ajax():
		
		Question.objects.filter(usuario_id = request.session["id"]).update(interesado = request.POST['interesado'],
			religion = request.POST['religion'],cocina = request.POST['cocina'],fiesta = request.POST['fiesta'],
			distancia = request.POST['distancia'],geo_quest = request.POST['geo_question'],geo = request.POST['geo'],
			hijo = request.POST['hijo'],bebida = request.POST['bebida'],fumas = request.POST['fumas'])

		return HttpResponse("0")

def insert_musica_question(request):
	if request.is_ajax():
		GeneroMusicalQuestion.objects.filter(question_id = request.POST['id_question']).delete()

		musica = request.POST['music']
		musica = musica.split(", ")
		
		for item in musica:
			
			try:
				musicData = GeneroMusical.objects.get(descripcion = item.strip())
				musicQuestion = GeneroMusicalQuestion(question_id = request.POST['id_question'], musica_id = musicData.id)
				musicQuestion.save()

			except Exception, e:
				error= ""

		return HttpResponse(musica)

def define_image_perfil(request):

	if request.is_ajax():

		foto = define_foto_perfil(request.POST['media_id'],request.session["id"])

		request.session["image"] = foto

		return HttpResponse("0")

def mis_pretendientes(request):
	try:

		usuario = Usuario.objects.filter(email = request.session["usuario"] ,estado = 1).select_related('ciudad__provincia__pais')

		id_usuario = request.session["id"]

		foto_perfil = get_foto_perfil(id_usuario)

		pretendientes = get_pretendientes(id_usuario)

		return render_to_response("pretendientes.html",{
			'usuario' : usuario,
			'foto_perfil': foto_perfil,
			'session':request,
			'pretendientes' : pretendientes
		})

	except Exception, e:
		return redirect('/')



			

		








	



	

	








