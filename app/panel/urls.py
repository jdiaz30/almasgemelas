from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$','panel.views.index',name='index'),
    url(r'^get-all-pais/','panel.views.get_all_pais',name='get_all_pais'),
    url(r'^get-prov-by-pais/(?P<pais>[_a-z0-9-])','panel.views.get_prov_by_pais',name='get_prov_by_pais'),
    url(r'^get-ciudad-by-prov/(?P<prov>[_a-z0-9-])','panel.views.get_ciudad_by_prov',name='get_ciudad_by_prov'),
   	url(r'^get-genero-musical/','panel.views.get_genero_musical',name='get_genero_musical'),
   	url(r'^upload-image/','panel.views.upload_image',name='upload_imagel'),
   	url(r'^delete-media/','panel.views.delete_media',name='delete_media'),
   	url(r'^define-image-perfil/','panel.views.define_image_perfil',name='define_image_perfil'),
   	url(r'^edit-usuario/','panel.views.edit_usuario',name='edit_usuario'),
   	url(r'^edit-question/','panel.views.edit_question',name='edit_question'),
 	  url(r'^insert-musica-question/','panel.views.insert_musica_question',name='insert_musica_question'),
    url(r'^mis-pretendientes/','panel.views.mis_pretendientes',name='mis_pretendientes'),
 	
)