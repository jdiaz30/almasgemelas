$(function() {
    var vPareja = "";
    var vReligion = "";
    var vCocinar = "";
    var vFiesta = "";
    var vHijo = "";
    var vBebida = "";
    var vFumas = "";
    var vUser = {};
    var vQuestion = {};
    var vThisMedia = null;
    var vRangoPerfil = null;
    var vDistancia = 0;
    var vGeo = ""; //lat,long

	var vPanel = {
		init : function(){

            $("[name='geo-question']").bootstrapSwitch();

            vRangoPerfil = new Slider('#rango-perfil', {
                formatter: function(value) {
                    return 'Distancia: ' + value;
                }
            });

            $('input[name="geo-question"]').on('switchChange.bootstrapSwitch', function(event, state) {

                if(state){
                    vQuestion.geo_question = "Si";
                    
                    vPanel.geolocalizacion();
                }else{
                    vQuestion.geo_question = "No";
                }

                setTimeout(function(){
                    vQuestion.geo = vGeo;
                    vPanel.editQuestion();
                },1000);    

            });

            vRangoPerfil.on('slideStop',function(){

               vQuestion.distancia = vRangoPerfil.getValue();

               vPanel.editQuestion();
            });

            $("#frmMedia").submit(function(e){

                e.preventDefault();

                vPanel.uploadMedia();

            });

            $("#id_image").change(function(){

                vPanel.uploadMedia();

            });

            $(document).on('click','.select-image', function() {
                vPanel.selectIdMedia(this);

            });

            $(document).on('click','.select-perfil', function() {
                vPanel.selectIdMedia(this);

            });

            $(".delete-image").click(function(){

                vPanel.deleteImage();

            });

            $(".perfil-image").click(function(){

                vPanel.imagePerfil();

            });

            vPanel.controlEditableInit();

            vPanel.parejaOption();
            vPanel.religionOption();
            vPanel.cocinarOption();
            vPanel.fiestaOption();
            vPanel.hijosOption();
            vPanel.bebidaOption();
            vPanel.fumasOption();

            vPanel.dataBinding();

            vPanel.calcularEdad();
            vPanel.verDataUsuario();

            ko.applyBindings(new vViewModel.usuarioViewModel(),document.getElementById("panel"));
            ko.applyBindings(new vViewModel.headerViewModel(),document.getElementById("header-data"));
		},
        controlEditableInit : function(){

            $('.editable').editable();

            $('#nombre').editable({
                validate: function(value) {

                  setTimeout(function(){
                        vUser.nombre(value);
                        vHeader.usuario_session(value);
                        vPanel.editUsuario();
                    },100);
                }
           });

            $('#titulo').editable({
                validate: function(value) {

                  setTimeout(function(){
                        vUser.titulo(value);
                        vPanel.editUsuario();
                    },100);
                }
           });

            $('#descripcion').editable({
                validate: function(value) {

                  setTimeout(function(){
                        vUser.descripcion(value);
                        vPanel.editUsuario();
                    },100);
                }
           });

            $('#fec_nac').editable({
                format: 'DD-MM-YYYY', 
                success : function(response, newValue){

                    setTimeout(function(){
                        vPanel.calcularEdad();
                        vPanel.editUsuario();
                    },100);
   
                }
            });

            $('#pais').editable({
                prepend: "elegir pais",
                source: "/panel/get-all-pais/",
                validate: function(value) {

                    vPanel.resetEditable("#prov","elegir provincia");
                    vPanel.resetEditable("#ciudad","elegir ciudad");
                    vPanel.provinciaEditable(value);
                }
           });

            $('#sexo').editable({
                prepend: "elegir sexo",
                source: [
                {value: 'm', text: 'Masculino'},
                {value: 'f', text: 'Femenino'}
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.sexo(value);
                        vPanel.editUsuario();
                    },100);

                }
            });

            $('#estado-civil').editable({
                prepend: "elegir",
                source: [
                {value: 's', text: 'Soltero'},
                {value: 'v', text: 'Viudo'},
                {value: 'd', text: 'Divorciado'}
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.est_civil(value);
                        vPanel.editUsuario();
                    },100);

                } 
            });

            $('#estatura').editable({
                prepend: "elegir",
                source: [
                {value: 'Alto', text: 'Alto'},
                {value: 'Medio', text: 'Medio'},
                {value: 'Bajo', text: 'Bajo'}
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.estatura(value);
                        vPanel.editUsuario();
                    },100);

                } 
            });

            $('#piel').editable({
                prepend: "elegir",
                source: [
                {value: 'Blanco', text: 'Blanco'},
                {value: 'Moreno', text: 'Moreno'},
                {value: 'Oscuro', text: 'Oscuro'}
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.piel(value);
                        vPanel.editUsuario();
                    },100);

                } 
            });

            $('#ojos').editable({
                prepend: "elegir",
                source: [
                {value: 'Marrones', text: 'Marrones'},
                {value: 'Negros', text: 'Negros'},
                {value: 'Verdes', text: 'Verdes'},
                {value: 'Azules', text: 'Azules'},
                {value: 'Pardos', text: 'Pardos'},
                {value: 'Otro', text: 'Otro'}                 
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.ojos(value);
                        vPanel.editUsuario();
                    },100);

                } 
            });

            $('#cabello').editable({
                prepend: "elegir",
                source: [
                {value: 'Negro', text: 'Negro'},
                {value: 'Rubio', text: 'Rubio'},
                {value: 'Rojo', text: 'Rojo'},
                {value: 'Plateado', text: 'Plateado'},
                {value: 'Castaño', text: 'Castaño'},
                {value: 'Otro', text: 'Otro'}                 
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.cabello(value);
                        vPanel.editUsuario();
                    },100);

                } 
            });

            $('#educacion').editable({
                prepend: "elegir",
                source: [
                {value: '0', text: 'No especificado'},
                {value: '1', text: 'Instituto de Enseñanza Superior'},
                {value: '2', text: 'Estudiante Universitario/a'},
                {value: '3', text: 'Titulo Universitario'},
                {value: '4', text: 'Estudios PostGrado/Master'},
                {value: '5', text: 'Entrenamiento Técnico'},
                {value: '6', text: 'Diplomado Universitario'},
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.educacion(value);
                        vPanel.editUsuario();
                     
                    },100);

                } 
            });

            $('#fisico').editable({
                prepend: "elegir",
                source: [
                {value: '1', text: 'Delgado/a'},
                {value: '2', text: 'Esbelto/a'},
                {value: '3', text: 'Musculoso/a'}
                ],
                validate : function(value){
                    setTimeout(function(){
                        vUser.fisico(value);
                        vPanel.editUsuario();
                    },100);
                } 
            });

            $('#ocupacion').editable({
                prepend: "elegir",
                source: [
                {value: '0', text: 'No especificado'},
                {value: '1', text: 'Independiente/Autónomo'},
                {value: '2', text: 'Manager'},
                {value: '3', text: 'Profesional'},
                {value: '4', text: 'Ejecutivo'},
                {value: '5', text: 'Administrativo'},
                {value: '6', text: 'Ventas'},
                {value: '7', text: 'Servicio al Cliente'},
                {value: '8', text: 'Comerciante'},
                {value: '9', text: 'Artista'},
                {value: '10', text: 'Profesor'},
                {value: '11', text: 'Ama de Casa/En casa con los hijos'},
                {value: '12', text: 'Desempleado'},
                ],
                validate : function(value){

                    setTimeout(function(){
                        vUser.ocupacion(value);
                        vPanel.editUsuario();
                     
                    },100);

                } 
            });

            $('#music').editable({
                inputclass: 'input-large',

                select2: {

                    tags: vPanel.generoMusical(),
                
                    tokenSeparators: [",", " "]
                },
                validate : function(value){
                    setTimeout(function(){

                        vPanel.editQuestionMusical();

                    },1000);

                    
                }
            });

            vPanel.cargarUbicacion();     
        },
        cargarUbicacion : function(){

            var vPais = $("#pais").attr("data-id");
            var vProv = $("#prov").attr("data-id");

            vPanel.provinciaEditable(vPais);
            vPanel.ciudadEditable(vProv);
        },
        parejaOption : function(){

            $("#pareja").click(function(){

                $(this).fadeOut(100,function(){

                    $(".pareja").fadeIn(100);

                });
 
            });

            $("input[name='pareja']").click(function(){

                vPareja = $(this).val();

                vQuestion.interes = vPareja;

                vPanel.editQuestion();

                $(".pareja").fadeOut(100,function(){

                    var vResult = (vPareja == "f") ? "Mujer" : "Hombre";

                    $("#pareja").html(vResult);

                    $("#pareja").fadeIn(100);

                });

            });
        },
        religionOption : function(){

            $("#religion").click(function(){

                $(this).fadeOut(100,function(){

                    $(".religion").fadeIn(100);

                });

            });

            $("input[name='religion']").click(function(){

                vReligion = $(this).val();
                var vResult = $(this).attr("data");

                vQuestion.religion = vReligion.toLowerCase();

                vPanel.editQuestion();

                $(".religion").fadeOut(100,function(){

                    $("#religion").html(vResult);

                    $("#religion").fadeIn(100);

                });

            });
        },
        cocinarOption : function(){

            $("#cocinar").click(function(){

                $(this).fadeOut(100,function(){

                    $(".cocinar").fadeIn(100);

                });

            });

            $("input[name='cocinar']").click(function(){

                vCocinar = $(this).val();

                vQuestion.cocina = vCocinar.toLowerCase();

                vPanel.editQuestion();

                $(".cocinar").fadeOut(100,function(){

                    $("#cocinar").html(vCocinar);

                    $("#cocinar").fadeIn(100);

                });

            });
        },
        fiestaOption : function(){

            $("#fiesta").click(function(){

                $(this).fadeOut(100,function(){

                    $(".fiesta").fadeIn(100);

                });

            });

            $("input[name='fiesta']").click(function(){

                vFiesta = $(this).val();

                vQuestion.fiesta = vFiesta;

                vPanel.editQuestion();

                $(".fiesta").fadeOut(100,function(){

                    vFiesta = (vFiesta== "Av")? "A veces": vFiesta;

                    $("#fiesta").html(vFiesta);

                    $("#fiesta").fadeIn(100);

                });

            });
        },
        hijosOption : function(){

            $("#hijos").click(function(){

                $(this).fadeOut(100,function(){

                    $(".hijos").fadeIn(100);

                });
 
            });

            $("input[name='hijo']").click(function(){

                vHijo = $(this).val();

                var vText = $(this).attr("data");

                vQuestion.hijo = vHijo;

                vPanel.editQuestion();

                $(".hijos").fadeOut(100,function(){

                    $("#hijos").html(vText);

                    $("#hijos").fadeIn(100);

                });

            });
        },
        bebidaOption : function(){

            $("#bebida").click(function(){

                $(this).fadeOut(100,function(){

                    $(".bebida").fadeIn(100);

                });
 
            });

            $("input[name='bebida']").click(function(){

                vBebida = $(this).val();

                var vText = $(this).attr("data");

                vQuestion.bebida = vBebida;

                vPanel.editQuestion();

                $(".bebida").fadeOut(100,function(){

                    $("#bebida").html(vText);

                    $("#bebida").fadeIn(100);

                });

            });
        },
        fumasOption : function(){

            $("#fumas").click(function(){

                $(this).fadeOut(100,function(){

                    $(".fumas").fadeIn(100);

                });
 
            });

            $("input[name='fumas']").click(function(){

                vFumas = $(this).val();

                var vText = $(this).attr("data");

                vQuestion.fumas = vFumas;

                vPanel.editQuestion();

                $(".fumas").fadeOut(100,function(){

                    $("#fumas").html(vText);

                    $("#fumas").fadeIn(100);

                });

            });
        },
        provinciaEditable : function(vPais){

            $('#prov').editable({
                prepend: "elegir provincia",
                source: "/panel/get-prov-by-pais/"+vPais,
                validate: function(value) {
                    vPanel.ciudadEditable(value);
                }
           });
        },
        ciudadEditable : function(vProv){

            $('#ciudad').editable({
                prepend: "elegir ciudad",
                source: "/panel/get-ciudad-by-prov/"+vProv,
                validate: function(value) {

                    setTimeout(function(){
                        vUser.ciudad(value);
                        vPanel.editUsuario();
                    },100);
                    
                }
           });
        },
        resetEditable : function(vId,vTexto){

            $(vId).editable('destroy');
            $(vId).html(vTexto);
        },
        calcularEdad : function(){

            var vFecha =  $('#fec_nac').html().trim();

            var vEdad = calcularEdad(vFecha);

            $("#edad_text").html(vEdad + " años");

            vPanel.verZodiaco(vFecha);
        },
        verZodiaco : function(vFecha){

            var vSigno = "";

            if (vFecha !== "") {
                vSigno = signoZodiaco(vFecha);
            }else{
                vSigno = "---";
            }

            $("#signo").html(vSigno);
        },
        dataBinding : function(){

            var vSexo = $("#sexo").html().trim().toLowerCase();
            var vEstCivil = $("#estado-civil").html().trim().toLowerCase();
            var vCiudad = $("#ciudad").attr("data-id");
            var vNombre = $("#nombre").html().trim();
            var vTitulo = $("#titulo").html().trim();
            var vDescripcion = $("#descripcion").html().trim();
            var vEstatura = $("#estatura").html().trim();
            var vPiel = $("#piel").html().trim();
            var vFotoPerfil = $("#foto_perfil").val();
            var vEducacion = $("#educacion_val").val();
            var vOcupacion = $("#ocupacion_val").val();
            var vOjos = $("#ojos").html().trim();
            var vCabello = $("#cabello").html().trim();
            var vFisico = $("#fisico_val").val();

            vPareja = ($("input[name='pareja']:checked").val() === undefined) ? "" : $("input[name='pareja']:checked").val();
            vReligion = ($("input[name='religion']:checked").val() === undefined) ? "" : $("input[name='religion']:checked").val();
            vCocinar = $("input[name='cocinar']:checked").val().toLowerCase();
            vFiesta = $("input[name='fiesta']:checked").val();
            vHijo = $("input[name='hijo']:checked").val();
            vBebida = $("input[name='bebida']:checked").val();
            vDistancia = vRangoPerfil.getValue();
            vFumas = $("input[name='fumas']:checked").val();
            var vGeoQuestion = "No";

            //vFiesta = (vFiesta== "Av")? "A veces": vFiesta;

            if (vSexo !=="") {
                vSexo = vSexo.substr(0,1);
            }

            if (vEstCivil !=="") {
                vEstCivil = vEstCivil.substr(0,1);
            }

            if ($('input[name="geo-question"]').bootstrapSwitch('state') === true) {
                vGeoQuestion = "Si";
            }

            vUser = {
                nombre :  ko.observable(vNombre),
                sexo : ko.observable(vSexo),
                est_civil : ko.observable(vEstCivil),
                ciudad : ko.observable(vCiudad),
                titulo : ko.observable(vTitulo),
                descripcion : ko.observable(vDescripcion),
                estatura : ko.observable(vEstatura),
                piel : ko.observable(vPiel),
                foto_perfil : ko.observable(vFotoPerfil),
                educacion : ko.observable(vEducacion),
                ocupacion : ko.observable(vOcupacion),
                ojos : ko.observable(vOjos),
                cabello : ko.observable(vCabello),
                fisico : ko.observable(vFisico)
            };

            vQuestion = {
                interes : vPareja,
                religion : vReligion,
                cocina : vCocinar,
                fiesta : vFiesta,
                distancia : vDistancia,
                geo_question : vGeoQuestion,
                geo : vGeo,
                hijo : vHijo,
                bebida : vBebida,
                fumas : vFumas
            };
        },
        generoMusical : function(){

            var vMusica  = [];

            $.ajax({
                url:"/panel/get-genero-musical/",  
                type:'GET',
                async:false, 
                dataType : 'json',
        
                success:function(respuesta){

                    $.each(respuesta, function(index, value){
                        vMusica.push(value.text);
                    });
                }
            });

            return vMusica;
        },
        uploadMedia : function(){

            var data = new FormData($('#frmMedia').get(0));

            $.ajax({
                url: '/panel/upload-image/',
                type: 'POST',
                data: data,
                dataType : 'json',
                cache: false,
                async:true, 
                processData: false,
                contentType: false,
                beforeSend : function(){
                    $(".btn.btn-1").html("subiendo ...");
                },
                success: function(data) {
                    $(".btn.btn-1").html("listo");
                    vPanel.addImageUpload(data);
                }
            });

            return false;
        },
        addImageUpload : function(vFileName){

            var vFile = vFileName.link.replace("panel", "");

            var vHtml = '<div class="col-lg-3 col-md-4 col-xs-6 thumb"><a class="thumbnail" href="#">';

            vHtml += '<img class="img-responsive" src="'+vFile+'" alt="">';
            vHtml +='<div style="text-align: center">' +
            '<i class="fa fa-trash-o select-image fa-iconos" data="'+vFileName.id+'"></i> <i class="fa fa-star-o select-perfil fa-iconos" data="'+vFileName.id+'"></i>' + 
            '</div>';

            vHtml += '</a></div>';

            $("#media-image-upload").append(vHtml);
        },
        selectIdMedia : function(vThis){

            var vIdMedia = $(vThis).attr("data");

            $("#media_id").val(vIdMedia);

            vThisMedia = vThis;

            if($(vThis).hasClass("select-perfil")){
                $("#mPerfilMedia").modal('show');
            }else{
                $("#mDeleteMedia").modal('show');
            }
        },
        deleteImage : function(){

            $( vThisMedia ).parents( ".thumb" ).remove();

            $("#mDeleteMedia").modal('hide');

            $.ajax({
                url:"/panel/delete-media/",  
                type:'POST',
                async:true, 
                data : {
                    media_id : $("#media_id").val()
                },

                success:function(respuesta){

                }
            });
        },
        imagePerfil : function(){

            $(".fa-star").removeClass("fa-star").addClass("fa-star-o").addClass("select-perfil");

            $(".select-perfil[data='" + $("#media_id").val() + "']").removeClass("fa-star-o");
            $(".select-perfil[data='" + $("#media_id").val() + "']").addClass("fa-star");

            var vThis = $(".select-perfil[data='" + $("#media_id").val() + "']").parent().parent();
            
            vUser.foto_perfil($(vThis).find('img').attr("src"));
            vHeader.foto_perfil($(vThis).find('img').attr("src"));

            $.ajax({
                url:"/panel/define-image-perfil/",  
                type:'POST',
                async:true, 
                data : {
                    media_id : $("#media_id").val()
                },

                success:function(respuesta){
                    $("#mPerfilMedia").modal('hide');
                }
            });
        },
        editUsuario : function(){

            $.ajax({
                url:"/panel/edit-usuario/",  
                type:'POST',
                async:true, 
                data : {
                    nombre : vUser.nombre(),
                    fecha_nac : convertFechaYmd($('#fec_nac').html().trim()),
                    sexo : vUser.sexo(),
                    est_civil : vUser.est_civil(),
                    ciudad : vUser.ciudad(),
                    titulo : vUser.titulo(),
                    descripcion : vUser.descripcion(),
                    estatura : vUser.estatura(),
                    piel : vUser.piel(),
                    educacion : vUser.educacion(),
                    ocupacion : vUser.ocupacion(),
                    ojos : vUser.ojos(),
                    cabello : vUser.cabello(),
                    fisico : vUser.fisico()
                },

                success:function(respuesta){

                }
            });
        },
        editQuestion : function(){

            $.ajax({
                url:"/panel/edit-question/",  
                type:'POST',
                async:true, 
                data : {
                    interesado : vQuestion.interes,
                    religion : vQuestion.religion,
                    cocina : vQuestion.cocina,
                    fiesta : vQuestion.fiesta,
                    distancia : vQuestion.distancia,
                    geo_question : vQuestion.geo_question,
                    geo : vQuestion.geo,
                    hijo : vQuestion.hijo,
                    bebida : vQuestion.bebida,
                    fumas : vQuestion.fumas
                },

                success:function(respuesta){

                }
            });
        },
        editQuestionMusical : function(){

            $.ajax({
                url:"/panel/insert-musica-question/",  
                type:'POST',
                async:true, 
                data : {
                    id_question : $("#id_question").val(),
                    music : $("#music").html().trim()
                },
                success:function(respuesta){
                    console.log(respuesta);
                }
            });
        },
        geolocalizacion : function(){

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position){
                    vGeo = position.coords.latitude +","+ position.coords.longitude;
                });
            } else {
                console.log("no se puede obtnener");
            }
        },
        verDataUsuario : function(){

            var vEducacion = getValueEducacion($("#educacion_val").val());
            var vOcupacion = getValueOcupacion($("#ocupacion_val").val());
            var vFisico = getValueFisico($("#fisico_val").val());

            $("#educacion").html(vEducacion);
            $("#educacion").fadeIn(100);

            $("#ocupacion").html(vOcupacion);
            $("#ocupacion").fadeIn(100);

            $("#fisico").html(vFisico);
        }
	};

    var vViewModel = {
        usuarioViewModel : function(){
            this.usuario = vUser;
        },
        headerViewModel : function(){
            this.header = vHeader;
        }
    };

    var vHeader = {
        foto_perfil : ko.observable($("#foto_perfil").val()),
        usuario_session : ko.observable($("#usuario_name_session").val())
    };

	vPanel.init();
});