from django.db import models

from home.models import Usuario

import os
import string
import random

from easy_thumbnails.fields import ThumbnailerImageField

#rename file upload
def content_file_name(instance, filename):
    ext = filename.split('.')[-1]
    text = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(32)])

    filename = "%s_%s.%s" % (instance.usuario.id, text, ext)
    return os.path.join('panel/static/img/usuarios/', filename)


class Religion(models.Model):
	nombre = models.CharField(max_length = 150) #deseas mostrar tu ubicacion
	estado = models.IntegerField()

	class Meta:
		db_table = 'tb_religion'
	def __str__(self):
		return self.nombre

class GeneroMusical(models.Model):
	descripcion = models.CharField(max_length = 150) #deseas mostrar tu ubicacion
	estado = models.IntegerField()

	class Meta:
		db_table = 'tb_genero_musical'
	def __str__(self):
		return self.descripcion

class Question(models.Model):
	usuario = models.ForeignKey(Usuario)
	geo_quest = models.CharField(max_length = 2,null = True) #deseas mostrar tu ubicacion
	geo = models.TextField(null = True)
	interesado = models.CharField(max_length = 2, null = True)
	cocina = models.CharField(max_length = 2, null = True)
	religion = models.ForeignKey(Religion, blank = True, null = True)
	fiesta = models.CharField(max_length = 2, null = True)
	distancia = models.IntegerField(blank=True, null = True)
	hijo = models.IntegerField()
	bebida = models.IntegerField()
	fumas = models.IntegerField()

	class Meta:
		db_table = 'tb_question'

class GeneroMusicalQuestion(models.Model):
	question = models.ForeignKey(Question)
	musica = models.ForeignKey(GeneroMusical)

	class Meta:
		db_table = 'tb_gm_question'

class Media(models.Model):
	usuario = models.ForeignKey(Usuario)
	#link = models.FileField(upload_to = content_file_name)
	link = ThumbnailerImageField(upload_to = content_file_name)
	tipo = models.CharField(max_length = 15)
	estado = models.IntegerField()
	perfil = models.CharField(max_length = 2 ,null = False)

	class Meta:
		db_table = 'tb_media'
		get_latest_by = "id"

class Pais(models.Model):
	nombre = models.CharField(max_length = 150)
	moneda = models.CharField(max_length = 5,null = True)
	iso = models.CharField(max_length = 3,null = True)
	url = models.CharField(max_length = 25,null = True)
	estado = models.IntegerField()

	class Meta:
		db_table = 'tb_pais'
	def __unicode__(self):
		return self.nombre

class Provincia(models.Model):
	nombre = models.CharField(max_length = 150)
	pais = models.ForeignKey(Pais)
	url = models.CharField(max_length = 25,null = True)
	estado = models.IntegerField()

	class Meta:
		db_table = 'tb_provincia'
	def __unicode__(self):
		return self.nombre

class Ciudad(models.Model):
	nombre = models.CharField(max_length = 150)
	provincia = models.ForeignKey(Provincia)
	url = models.CharField(max_length = 25,null = True)
	estado = models.IntegerField()

	class Meta:
		db_table = 'tb_ciudad'
	def __unicode__(self):
		return self.nombre
