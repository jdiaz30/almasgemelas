from django.contrib import admin

from panel.models import Religion,GeneroMusical,Question,GeneroMusicalQuestion,Media,Ciudad,Pais,Provincia


admin.site.register(Religion)
admin.site.register(GeneroMusical)
admin.site.register(GeneroMusicalQuestion)
admin.site.register(Media)
admin.site.register(Ciudad)
admin.site.register(Pais)
admin.site.register(Provincia)