# -*- coding: utf-8 -*-  
import json
import datetime
import time

def calcular_edad(fecha):
	yeardays = 365
	act_year = datetime.date.today().year
	diff = (datetime.date.today() - fecha).days
	if act_year / 4 == 0 and act_year != 100 or act_year / 400 == 0:
		yeardays += 1
	else:
		yeardays = 365
		years = str(int(diff/yeardays))

	return years
	
def calcular_signo_zodiaco(fecha):

	signo = ("Capricornio", "Acuario", "Piscis", "Aries", "Tauro", "Geminis", "Cancer", "Leo", "Virgo", "Libra", "Escorpio", "Sagitario")

	fechas = (20, 19, 20, 20, 21, 21, 22, 22, 22, 22, 22, 21)

	dia = int(fecha[-2:])
	mes = int(fecha[5:7]) -1

	if dia > fechas[mes]:
		mes=mes+1
	if mes==12:
		mes=0

	return  signo[mes]

def calcular_dias(fecha_ini ,fecha_fin):

	fecha_ini = datetime.datetime.strptime(fecha_ini, '%Y-%m-%d')
	fecha_fin = datetime.datetime.strptime(fecha_fin, '%Y-%m-%d')
	total = fecha_ini - fecha_fin

	if total.days >=365:
		anio = total.days / 365
		if int(anio) == 1 :
			response = "Hace 1" + " año"
		else:
			response = str(anio) + " años"

	else:
		if total.days == 0 :
			response = "hoy"
		elif total.days <= 31 :
			if(total.days == 1):
				response = "hace " + str(total.days) + " dia"
			else:
				response = "hace " + str(total.days) + " dias"
			
		else :
			mes = total.days / 30
			if int(mes) == 1:
				response = "hace 1 mes"
			else :
				response = "hace " + str(mes) + " meses"


	return response

