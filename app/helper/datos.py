# -*- coding: utf-8 -*-  
import json
import datetime
import time

def parser_estado_civil(value):
	if value == None or value == "" or value == "e":
		value = "n"
	estado_civil = {'s': 'Soltero','v':'Viudo','d':'Divorciado','n' :'-----'}
	return estado_civil[value]

def parser_fisico_usuario(value):
	if value == 'None' or value == "":
		value = "0"
	fisico = {'0' :'-----','1': 'Delgado/a','2':'Esbelto/a','3':'Musculoso/a'}
	return fisico[value]

def parser_educacion(value):
	if value == 'None' or value == "":
		value = "0"
	educacion = {'0' :'No especificado','1': 'Instituto de Enseñanza Superior','2':'Estudiante Universitario/a','3':'Titulo Universitario',
	'4':'Estudios PostGrado/Master','5':'Entrenamiento Técnico','6':'Diplomado Universitario'}
	return educacion[value]

def parser_ocupacion(value):
	if value == 'None' or value == "":
		value = "0"
	ocupacion = {'0' :'No especificado','1': 'Independiente/Autónomo','2':'Manager','3':'Profesional','4':'Ejecutivo',
	'5':'Administrativo','6':'Ventas','7':'Servicio al Cliente','8':'Comerciante','9':'Artista',
	'10':'Profesor','11':'Ama de Casa/En casa con los hijos','12':'Desempleado'}
	return ocupacion[value]

def parser_nombre_session(value):
	espacio_blanco_pos = value.find(" ")
	return value[0:espacio_blanco_pos]